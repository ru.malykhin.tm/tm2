package ru.malykhin.tm.service;

import ru.malykhin.tm.entity.Task;
import ru.malykhin.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(String name) {
        return taskRepository.create(name);
    }

    public Task findByIndex(int index) {
        return taskRepository.findByIndex(index);
    }

    public Task findByName(String name) {
        return taskRepository.findByName(name);
    }

    public void load(Task task) {
        taskRepository.load(task);
    }

    public Task removeById(Long id) {
        return taskRepository.removeById(id);
    }

    public Task removeByName(String name) {
        return taskRepository.removeByName(name);
    }

    public Task findById(Long id) {
        return taskRepository.findById(id);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

}
