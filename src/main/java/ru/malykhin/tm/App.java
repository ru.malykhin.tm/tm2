package ru.malykhin.tm;

import ru.malykhin.tm.controller.ProjectController;
import ru.malykhin.tm.controller.SystemController;
import ru.malykhin.tm.controller.TaskController;
import ru.malykhin.tm.repository.ProjectRepository;
import ru.malykhin.tm.repository.TaskRepository;
import ru.malykhin.tm.service.ProjectService;
import ru.malykhin.tm.service.TaskService;

import java.util.Scanner;

import static ru.malykhin.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskService taskService = new TaskService(taskRepository);

    private final TaskController taskController = new TaskController(taskService);

    private final SystemController systemController = new SystemController();

    {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        taskRepository.create("TEST TASK 1");
        taskRepository.create("TEST TASK 2");
    }

    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return  systemController.displayAbout();
            case HELP: return  systemController.displayHelp();
            case SAVE: return  systemController.saveAll(projectController,taskController);
            case LOAD: return  systemController.loadAll(projectController,taskController);
            case EXIT: return  systemController.displayExit();


            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_SAVE: return projectController.saveProject();
            case PROJECT_LOAD: return projectController.loadProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case TASK_LIST: return taskController.listTask();
            case TASK_SAVE: return taskController.saveTask();
            case TASK_LOAD: return taskController.loadTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();

            default: return systemController.displayError();
        }
    }

}
