package ru.malykhin.tm.entity;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Task")
@XmlType(propOrder = {"id", "name", "description"})
public class Task {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    @XmlAttribute
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
