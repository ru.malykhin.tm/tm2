package ru.malykhin.tm.controller;

import ru.malykhin.tm.entity.Task;
import ru.malykhin.tm.service.TaskService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TaskController extends AbstractController{

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        taskService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: "+ task.getId());
        System.out.println("NAME: "+ task.getName());
        System.out.println("DESCRIPTION: "+ task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("ENTER, TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        int index = 1;
        for (final Task task: taskService.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    @XmlRootElement
    private static class Tasks {

        private List<Task> list;

        @XmlElement(name = "Task")
        public List<Task> getList() {
            return list;
        }

        public void setList(List<Task> list) {
            this.list = list;
        }

    }

    public int loadTask() {
        System.out.println("[LOAD TASK]");
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Tasks.class);
            Unmarshaller un = jaxbContext.createUnmarshaller();
            Tasks tsk = (Tasks) un.unmarshal(new File("BD.xml"));
            clearTask();
            for (final Task task: tsk.getList()) {
                taskService.load(task);
            }
            System.out.println("load complete");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println("[OK]");
        return 0;
    }

    public int saveTask() {
        System.out.println("[SAVE TASK]");
        try {
            Tasks tsk = new Tasks();
            tsk.setList(taskService.findAll());
            JAXBContext context = JAXBContext.newInstance(Tasks.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            marshaller.marshal(tsk, new File("BD.xml"));
            System.out.println("save complete");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println("[OK]");
        return 0;
    }



}
